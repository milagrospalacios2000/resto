<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="webside icon" type="ico" href="Imagenes/icono.ico">

    <title>Pedidos realizados</title>
    <link rel="stylesheet" href="style.CSS">  <!-- enlazo la hoja de estilos de css con el html -->
</head>

     <!-- HEADER -->  
     <header>
        <div class="logo">
            <img src="Imagenes/logo.png" alt="Logo">
        </div>
    </header>

    <div class='comida-items'>
<?php 
include 'conexionBD.php';
$sql = "SELECT * FROM `pedidos` P INNER JOIN `items_menu` IT ON (P.idItemMenu = IT.id) ORDER BY fechaAlta DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    for ($i = 0; $i < $result->num_rows; $i++) {
        $row = $result->fetch_assoc();
?>
        <div class='item'>
            <h2><?php echo $row['nombre']; ?></h2>
            <img src='mostrarImagen.php?id=<?php echo $row['id']; ?>' />
            <h3>Comentarios: <?php echo $row['comentarios']; ?></h3>
            <h3>Fecha: <?php echo $row['fechaAlta']; ?></h3>
            <h3>Precio: $<?php echo $row['precio']; ?></h3>
        </div>
<?php
    }
} else {
    echo "No se encontraron elementos en la base de datos.";
}
?>
</div>  
   
 <!-- FOORTER -->     
 <footer>
 <p class="footer"> Gracias por visitarnos </p>          
</footer> 
</body>
</html>