<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Chimichanga</title>

    <link rel="webside icon" type="ico" href="Imagenes/icono.ico">
   
    <link rel="stylesheet" href="style.CSS">  <!-- enlazo la hoja de estilos de css con el html -->
    
</head>
<body>

    <!-- HEADER -->  
    <header>
        <div class="logo">
            <img src="Imagenes/logo.png" alt="Logo">
        </div>
        <!-- BOTON PEDIR --> 
        <a href="/dashboard/altapedido.php"><button class="botonPedir">PEDIR</button></a>
    </header>
    <?php 
    session_start(); // verifica si hay un mensaje almacenado en la sesión
        if (isset($_SESSION['mensaje'])) {
            echo '<h2 class="mensaje">' . $_SESSION['mensaje'] . '</h2>';
            unset($_SESSION['mensaje']); // Elimina el mensaje de la sesión
    }
    ?>


    
    <!-- FORMULARIO -->  

    <form id="Formulario-filtrar" method="GET">

        <div class="Buscar">
            <input id="buscarInput" type="text" placeholder="Buscar" name="buscar">
            
        </div>
    
        <div class="desplegables">

            <div class="desplegable1"> 
                <select id="filtroMenu" name = "Fcomida">
                    <option disabled selected>Filtrar por comida</option> 
                    <option value="COMIDA"> Comida </option>
                    <option value="BEBIDA"> Bebidas </option>
                </select>
            </div>

            <div class=" desplegable2">
                <select id="filtroPrecio" name= "precio">
                    <option disabled selected>Ordenar por precio</option>
                    <option value="Ascendente"> Ascendente </option>
                    <option value="Descendente"> Descendente </option>
                </select>
            </div>

        </div>

        <div class="boton-filtrar">
            <button id="boton_filtro" type="submit">FILTRAR</button>
        </div>
    </form>
    
    
<?php 
include 'conexionBD.php';
// Obténgo los parámetros del formulario 
//isset() para verificar si la clave existe antes de intentar acceder a ella.
$buscar = isset($_GET['buscar']) ? $_GET['buscar'] : '';
$filtroComida = isset($_GET['Fcomida']) ? $_GET['Fcomida'] : '';
$ordenarPrecio = isset($_GET['precio']) ? $_GET['precio'] : '';

// Construye la consulta SQL
$sql = "SELECT * FROM items_menu WHERE 1=1";

if (!empty($buscar)) {
    $sql .= " AND NOMBRE LIKE '%$buscar%' ";
}

if (!empty($filtroComida)) {
    $sql .= " AND TIPO = '$filtroComida'";
}

if ($ordenarPrecio === 'Ascendente') {
    $sql .= " ORDER BY precio ASC";
} elseif ($ordenarPrecio === 'Descendente') {
    $sql .= " ORDER BY precio DESC";
}

// Ejecuta la consulta SQL y muestra los resultados en formato HTML
$result = $conn->query($sql);
if (!$result) {
    die("Error en la consulta: " . $conn->error);
}
?> <div class='comida-items'> 
<?php
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
?>
        <div class='item'>
            <img src='mostrarImagen.php?id=<?php echo $row['id']; ?>' />
            <h2><?php echo $row['nombre']; ?></h2>
            <h3>Precio: $<?php echo $row['precio']; ?></h3>
        </div>
<?php
    }
} else {
    echo " No se encontraron resultados.";
}
?>
</div>
    
</body>
 <!-- FOORTER  -->     
 <footer>
 <p class="footer"> Gracias por visitarnos </p>          
</footer>
</html>