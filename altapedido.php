<!DOCTYPE html>
<html lang="en">
<head>
   
    <link rel="webside icon" type="ico" href="Imagenes/icono.ico">

    <title>Pedidos</title>

    <link rel="stylesheet" href="style.CSS">  <!-- enlazo la hoja de estilos de css con el html -->
    <script type="text/javascript" src="script.js"></script> <!--enlazo la logica de javascrpt-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> <!-- Importe de alertas -->

</head>
<body>
     <!-- HEADER -->  
     <header>
        <div class="logo">
            <img src="Imagenes/logo.png" alt="Logo">
        </div>
       
    </header>

<?php include 'conexionBD.php'; ?>
        
    <!-- FORMULARIO --> 
     
    <form id="Formulario-altaPedido" method="GET" action="validar.php" onSubmit="return validarFunciones(this)">

        <div class="altaPedido">

            <div class="selec_comida">
                <h1>REALIZAR PEDIDO</h1>
            </div>

            <div class="desplegable-comida">
                <select id="menu" name="menu"> 
                <option disabled selected value="">Menú</option>
                    <?php                            
                    $sql = "SELECT * FROM items_menu WHERE 1=1";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        for ($i = 0; $i < $result->num_rows; $i++) {
                            $row = $result->fetch_assoc();
                            $id = $row['id'];
                            $nombre = $row['nombre'];
                            echo "<option value='$id'>$nombre</option>";
                        }
                    } 
                    else {
                        echo "No se encontraron elementos en la base de datos.";
                    }
                    ?>
                </select>
            </div>

            <div class="desplegable-mesas">
                <select id= "mesa" name="mesa">
                    <option disabled selected value="">Mesas</option>
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4 </option>
                    <option value="5">5 </option>
                    <option value="6"> 6 </option>
                    <option value="7"> 7 </option>
                    <option value="8"> 8 </option>
                    <option value="9"> 9 </option>
                    <option value="10"> 10 </option>
                </select>  
            </div>

            <div class="Fnotas" >
                <input type="text" placeholder="Nota (opcional)" id="notas" name="notas"> 
            </div>

            <div class="submit">
                <input type="submit"  value="Pedir">
            </div>
        </div>   
    </form>

</body>
   <!-- FOORTER -->     
   <footer>
    <p class="footer"> Gracias por visitarnos </p>          
</footer>
</html>